﻿---------------------------------------------------------------------------------------------------
Version: 1.0.5
Date: 2019-07-07
  Notes:
    - This release for 1.0
---------------------------------------------------------------------------------------------------
Version: 1.0.4
Date: 2019-07-07
  Notes:
    - This release for 0.18
---------------------------------------------------------------------------------------------------
Version: 1.0.3
Date: 2019-07-07
  Notes:
    - This release for 0.17
---------------------------------------------------------------------------------------------------
Version: 1.0.2
Date: 2019-07-07
  Notes:
    - This release for 0.16
---------------------------------------------------------------------------------------------------
Version: 1.0.1
Date: 2019-07-07
  Notes:
    - This release for 0.15
---------------------------------------------------------------------------------------------------
Version: 1.0.0
Date: 2019-07-07
  Notes:
    - This release for 0.14
